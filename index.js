//Using express framework to create API
const express = require('express')
// const path = require('path')
const cors = require('cors')

const searchNse = require('./searchNse.js')
const list = require('./searchStock.js')
const PORT = process.env.PORT || 5000
async function handler(){
    const exp = express();
    exp.use(cors())
    exp.get('/get',async (req, res) => res.send(await searchNse.searchStock(req)))
    exp.get('/list',async (req, res) => res.send(await list.listStock(req)))
    exp.listen(PORT, () => console.log(`Listening on ${ PORT }`))
}
handler()