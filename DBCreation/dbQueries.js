// Server: sql6.freemysqlhosting.net
// Name: sql6434503
// Username: sql6434503
// Password: mQG773rY5L
// Port number: 3306


//This file was used initially for creating db and inserting values to it

const mysql = require('mysql');
const csvtojson = require('csvtojson');
const con = mysql.createConnection({
  host: "sql6.freemysqlhosting.net",
  user: "sql6434503",
  password: "mQG773rY5L",
  database: "sql6434503"
});
function createTable(){
    con.connect(function(err) {
        if (err) throw err;
        var sql = `CREATE TABLE riafy_nse (slno INT AUTO_INCREMENT PRIMARY KEY
          ,name VARCHAR(255)
          , currentMarketPrice FLOAT
          ,marketCap FLOAT
          ,stockPE FLOAT
          ,dividendYield FLOAT
          ,roce FLOAT
          ,ROEPreviousAnnum FLOAT
          ,debtToEquity FLOAT
          ,eps FLOAT
          ,reserves FLOAT
          ,dept FLOAT)
          `;
        con.query(sql, function (err, result) {
          if (err) throw err;
          console.log("Table created");
          con.end();
        });
      });
}
function insertToTable(){
    const fileName = "nseDB.csv";
  
    csvtojson().fromFile(fileName).then(source => {
    

        for (var i = 0; i < source.length; i++) {
            var slno = source[i]["slno"],
                name = source[i]["name"],
                currentMarketPrice = source[i]["currentMarketPrice"],
                marketCap = source[i]["marketCap"],
                stockPE = source[i]["stockPE"],
                dividendYield = source[i]["dividendYield"],
                roce = source[i]["roce"],
                ROEPreviousAnnum = source[i]["ROEPreviousAnnum"],
                debtToEquity = source[i]["debtToEquity"],
                eps = source[i]["eps"],
                reserves = source[i]["reserves"],
                debt = source[i]["debt"]
    
            var insertStatement = 
            `INSERT INTO riafy_nse values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
            var items = [slno,name,currentMarketPrice,marketCap,stockPE,dividendYield,roce,ROEPreviousAnnum,debtToEquity,eps,reserves,debt];

            con.query(insertStatement, items, 
                (err, results, fields) => {
                if (err) {
                    console.log(
        "Unable to insert item at row ", i + 1);
                    return console.log(err);
                }
            });
        }
        console.log(
    "All items stored into database successfully");
    con.end();
    });
}
function dropTable(){
    con.connect(function(err) {
        if (err) throw err;
        var sql = "DROP TABLE riafy_nse";
        con.query(sql, function (err, result) {
          if (err) throw err;
          console.log("Table deleted");
          con.end();
        });
      });
}
// createTable()
// insertToTable()

//login table

function createLoginTable(){
  con.connect(function(err) {
      if (err) throw err;
      var sql = `CREATE TABLE users (slno INT AUTO_INCREMENT PRIMARY KEY
        ,username VARCHAR(255)
        ,password VARCHAR(255) )
        `;
      con.query(sql, function (err, result) {
        if (err) throw err;
        console.log("Table created");
        con.end();
      });
    });
}

// createLoginTable()